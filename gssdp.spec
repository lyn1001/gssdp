Name:          gssdp
Version:       1.6.3
Release:       2
Summary:       Resource discovery and announcement over SSDP
License:       LGPLv2+
URL:           http://www.gupnp.org/
Source0:       http://download.gnome.org/sources/gssdp/1.6/gssdp-%{version}.tar.xz

BuildRequires: chrpath gtk-doc pkgconfig vala >= 0.20 gobject-introspection-devel >= 1.36 meson
BuildRequires: dbus-glib-devel GConf2-devel glib2-devel gtk3-devel libsoup3-devel libxml2-devel
BuildRequires: gtk4-devel gi-docgen glib2-devel

Requires:      dbus
Provides:      gssdp-utils
Obsoletes:     gssdp-utils

%description
A GObject-based API for handling resource discovery and announcement over SSDP.

%package       devel
Summary:       Development files for gssdp
Requires:      gssdp = %{version}-%{release}

%description   devel
This package contains development files for gssdp.

%package       help
Summary:       Documentation files for gssdp
Requires:      gssdp = %{version}-%{release}
BuildArch:     noarch
Provides:      gssdp-docs = %{version}-%{release}
Obsoletes:     gssdp-docs < %{version}-%{release}

%description   help
This package contains documentation files for gssdp.

%prep
%autosetup -p1

%build
%meson -Dgtk_doc=true \
       -Dmanpages=false
%meson_build

%install
%meson_install
%delete_la

chrpath --delete %{buildroot}%{_bindir}/gssdp-device-sniffer

%check
%meson_test

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%license COPYING
%doc AUTHORS NEWS README.md
%{_libdir}/libgssdp-1.6.so.*
%{_libdir}/girepository-1.0/GSSDP-1.6.typelib
%{_bindir}/gssdp-device-sniffer

%files devel
%{_includedir}/gssdp-1.6/
%{_libdir}/libgssdp-1.6.so
%{_libdir}/pkgconfig/gssdp-1.6.pc
%{_datadir}/gir-1.0/GSSDP-1.6.gir
%{_datadir}/vala/vapi/gssdp*

%files help
%{_datadir}/doc/gssdp-1.6/

%changelog
* Thu Mar 7 2024 liyanan <liyanan61@h-partners.com> - 1.6.3-2
- Remove old so

* Wed Jan 24 2024 maqi <maqi@uniontech.com> - 1.6.3-1
- Upgrade 1.6.3

* Mon Aug 07 2023 xu_ping <707078654@qq.com> - 1.6.2-1
- Upgrade 1.6.2

* Fri May 27 2022 xu_ping <xuping33@h-partners.com> - 1.4.0.1-1
- Upgrade 1.4.0.1

* Mon Jun 7 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 1.2.3-1
- Upgrade to 1.2.3
- Update Version, Release, Source0, BuildRequires
- Update stage 'build', 'install', 'check' and 'files'

* Wed Oct 30 2019 Lijin Yang <yanglijin@huawei.com> - 1.0.2-7
- Initial package
